import React from "react";
import ReactDOM from "react-dom/client";
import "./index.scss";
import App from "./App";
import {reducers} from './bootstrap/reducers'
import { applyMiddleware, createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist'
import reportWebVitals from "./reportWebVitals";
import createSagaMiddleware from "redux-saga";
import { bootstrapSaga } from "./bootstrap/sagas";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { persistConfig } from "./storage/configureStore";
const root = ReactDOM.createRoot(document.getElementById("root"));
const sagaMiddleware = createSagaMiddleware();
const persistedReducer = persistReducer(persistConfig, reducers);
const store = createStore(persistedReducer, {}, applyMiddleware(sagaMiddleware))
const persistor = persistStore(store)

sagaMiddleware.run(bootstrapSaga);
root.render(
  <React.StrictMode>
    <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <App />
    </PersistGate>
    </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
