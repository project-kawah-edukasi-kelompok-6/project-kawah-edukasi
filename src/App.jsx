import React, { useEffect } from "react";
import "./App.scss";
import Home from "./components/pages/Home";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Pokemon from "./components/pages/Pokemon";
import About from "./components/pages/About";
import Favorites from "./components/pages/Favorites";
import Navbar from "./components/molekul/Navbar";
import Footer from "./components/molekul/Footer";
import NotFound from "./components/pages/NotFound";
import PokemonDetails from "./components/pages/PokemonDetails";
import { useDispatch, useSelector } from "react-redux";
import { getPokemon } from "./bootstrap/actions";
function App() {
  const dispatch = useDispatch();
  const { getPokemonFetch } = useSelector((state) => state);
  useEffect(() => {
    dispatch(getPokemon());
    console.log({ getPokemonFetch });
  }, [dispatch]);
  return (
    <div className="app">
      <Router>
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/pokemon" element={<Pokemon />} />
          <Route path="/pokemon/:id" element={<PokemonDetails />} />
          <Route path="/about" element={<About />} />
          <Route path="/favorites" element={<Favorites />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
