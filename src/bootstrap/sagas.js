import { all, call, put, takeLatest } from 'redux-saga/effects';
import * as CONST from './constants';
import * as ACTION from './actions';
import * as API from './apis';

function* getPokemon() {
  try {
    const api = API.getPokemonAPI
    const response = yield call(api);
    const newPokemon = response?.data?.result.map((e, i) => ({
      ...e,
      id: i + 1,
    }));
    yield put(ACTION.getPokemonSuccess(newPokemon));
  } catch (error) {
    console.log({ error });

    yield put(ACTION.getPokemonFailed(error));
  }
}

function* setFavPokemon(payload) {
  const {favorite , data} = payload.payload
  let newData = []
  const isAvaliable = favorite.find((element) => element.id === data.id)
  if (isAvaliable) {
    newData = favorite.filter((element) =>
  element.id !== data.id)
      console.log(newData)
    } else {
      newData = [...favorite, data]
      console.log(newData)
  }
    yield put(ACTION.SetPokemonSucess(newData));
 
  
}

const bootstrap = [takeLatest(CONST.GET_POKEMON, getPokemon), takeLatest(CONST.SET_FAVORITE, setFavPokemon)];

export function* bootstrapSaga() {
  yield all([...bootstrap]);
}
