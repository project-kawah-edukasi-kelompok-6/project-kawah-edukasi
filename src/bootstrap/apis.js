import axios from 'axios';
const BASE_URL = 'https://pokedexbackend.herokuapp.com';

const axioscall = axios.create({
    baseURL: BASE_URL,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });

const getPokemonAPI = () => {
  return axioscall.get(`/pokemon`);
}

export {axioscall ,getPokemonAPI}