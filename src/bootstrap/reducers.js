import { initialState } from "./initialStates";
import * as CONST from "./constants";

export function reducers(state = initialState, actions) {
  const { type, payload } = actions;

  switch (type) {
    case CONST.SET_USER:
      return { ...state, name: payload.name, age: payload.age };

    case CONST.GET_POKEMON:
      return { ...state, getPokemonFetch: true };
    case CONST.GET_POKEMON_SUCCESS:
      return { ...state, pokemon: payload };
    case CONST.SET_FAVORITE_SUCCESS:
      return { ...state, favorite: payload };
    case CONST.GET_POKEMON_FAILED:
      return { ...state, getPokemonError: payload };
    default:
      return state;
  }
}
