import * as CONST from './constants';

export function setName(payload) {
  return {
    type: CONST.SET_NAME,
    payload,
  };
}
export function setAge(payload) {
  return {
    type: CONST.SET_AGE,
    payload,
  };
}
export function setUser(payload) {
  return {
    type: CONST.SET_USER,
    payload,
  };
}
export function FavPokemon(payload) {
  return {
    type: CONST.SET_FAVORITE,
    payload,
  };
}
export function SetPokemonSucess(payload) {
  return {
    type: CONST.SET_FAVORITE_SUCCESS,
    payload,
  };
}

export function getPokemon(payload) {
  return {
    type: CONST.GET_POKEMON,
    payload,
  };
}
export function getPokemonSuccess(payload) {
  return {
    type: CONST.GET_POKEMON_SUCCESS,
    payload,
  };
}
export function getPokemonFailed(payload) {
  return {
    type: CONST.GET_POKEMON_FAILED,
    payload,
  };
}
