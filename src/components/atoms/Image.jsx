import React, { useEffect, useState } from "react";

const Images = ({ api = true, ...props }) => {
  const [imagesrc, setimagesrc] = useState(
    "/assets/image/default.png" || props.src
  );
  useEffect(() => {
    const img = new Image();
    img.src = props.src;
    img.onload = () => {
      setimagesrc(props.src);
    };
  }, [props.src]);
  return (
    <img
      src={api ? imagesrc : `/assets/image/${props.src}`}
      alt="Waiting Images"
      className={props.className}
      loading="lazy"
    />
  );
};

export default Images;
