import React from "react";
import { Images } from "../atoms";

const BannerOne = ({ ...props }) => {
  return (
    <div className="row p-3">
      <div className="contentone col-12 col-md-6 px-3">
        <p className="title">
          Pokedex <br />
          Is Coming <br />
          Come On Join
        </p>
        <p className="subtitle">
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Hic totam
          aliquam facere ad repudiandae qui nisi assumenda
        </p>
      </div>
      <div className="imageone d-none d-sm-block col-md-6">
        <Images api={false} src="undraw_online_test.svg" class="img-fluid" />
      </div>
    </div>
  );
};

export default BannerOne;
