import React from "react";
import { Cards } from "../molekul";
import { useNavigate } from "react-router-dom";
const BannerThree = ({ data }) => {
  const navigate = useNavigate();
  return (
    <div className="row justify-content-center p-4">
      {data?.length ? (
        data?.map((data, i) => {
          if (i <= 3) {
            return (
              <div
                key={data.id}
                className="col-12 col-md-2 d-flex justify-content-center"
              >
                <Cards
                  ids={data.id}
                  title={data?.name}
                  hidden={true}
                  flipper={true}
                  dualimages={{
                    front: data?.pictureFront,
                    back: data?.pictureBack,
                  }}
                  api={true}
                />
              </div>
            );
          }
        })
      ) : (
        <div className="loading">
          <div className="spinner-border text-primary" role="status"></div>
          <span className="visually text-primary ms-4">Loading...</span>
        </div>
      )}
      <div className="col-10 d-flex justify-content-center mt-5">
        <button
          onClick={() => navigate("/pokemon")}
          className="btn text-center text-underline viewmore"
        >
          View More
        </button>
      </div>
    </div>
  );
};

export default BannerThree;
