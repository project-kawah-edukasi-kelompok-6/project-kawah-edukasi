import React from "react";

const BannerTwo = ({ ...props }) => {
  return (
    <div className="row justify-content-center bg-second">
      <div className="contentwo col-12 col-md-6 ">
        <p className="title-2 text-center">
          We World Pokemon Is Biggest <br />
          Pokemon Go
        </p>
        <p className="subtitle text-center">
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Hic totam
          aliquam facere ad repudiandae qui nisi assumenda
        </p>
      </div>
    </div>
  );
};

export default BannerTwo;
