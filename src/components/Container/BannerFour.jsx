import React from "react";
import { Images } from "../atoms";
const BannerFour = ({ ...props }) => {
  return (
    <div className="row pt-3 bg-second">
      <div className="imageone 	d-none d-sm-block col-md-6">
        <Images api={false} src="undraw_coffee_with_friends.svg" />
      </div>
      <div className="contentone col-12 col-md-6">
        <p className="title">
          Is Tertarik <br />
          Come On Join
        </p>
        <p className="subtitle">
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Hic totam
          aliquam facere ad repudiandae qui nisi assumenda
        </p>
      </div>
    </div>
  );
};

export default BannerFour;
