import { useEffect } from "react";
import { Cards } from "../molekul";
import { useDispatch, useSelector } from "react-redux";
import { FavPokemon } from "../../bootstrap/actions";

const Favorites = () => {
  const { favorite } = useSelector((state) => state);
  const dispacth = useDispatch();
  const setFavorite = (data) => {
    dispacth(FavPokemon({ favorite, data }));
  };
  useEffect(() => {}, [favorite]);

  return (
    <div className="main row align-item-center justify-content-center">
      <div className="col-10 row gap-5">
        {favorite.length ? (
          favorite.map((item, index) => {
            return (
              <div className="col-12 col-md-3" key={index}>
                <Cards
                  ids={item.id}
                  title={item?.name}
                  onSetFavorite={setFavorite}
                  flipper={true}
                  dualimages={{
                    front: item?.pictureFront,
                    back: item?.pictureBack,
                  }}
                  data={item}
                  api={true}
                  favorite={true}
                ></Cards>
              </div>
            );
          })
        ) : (
          <div className="loading">Favorite Empty</div>
        )}
      </div>
    </div>
  );
};

export default Favorites;
