import React, { useEffect } from "react";
import { useLocation } from "react-router-dom";
import { Images } from "../atoms";
const About = () => {
  const location = useLocation();
  useEffect(() => {
    const title = location.pathname.split("/");
    document.title = title[1];
  }, []);
  return (
    <div className="main container py-4">
      <div className="row justify-content-center">
        <div className="col-md-2">
          <Images api={false} src="irvan.jpeg" className="img-thumbnail" />
          <div className="mt-3">
            <div className="skills mt-3">
              <a
                href="https://www.linkedin.com/in/mhirvan/"
                target="_blank"
                rel="noreferrer"
              >
                <Images
                  api={false}
                  src="linkedin.png"
                  className="sosial-icon"
                />
              </a>
              <div className="mb-0 ms-3">
                <h5 className="mb-0 text-muted">mhirvan</h5>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-6">
          <h3 className="mb-0">Mohammad Irvan</h3>
          <h6 className="text-muted mb-0">
            Frontend Developer | Physic, Univeristas Airlangga, IPK 3,01
          </h6>
          <p className="mt-1 mb-4 font-poppins text-start">
            I am a physics student at Airlangga University(UNAIR), I have skills
            in Web development with React and Vue, I have experience learning
            development from various courses and are self-taught, and in my
            department I was taught with strong logic. i do focus on web
            development field as frontend developer
          </p>
          <div>
            <div className="skills">
              <h5 className="mb-0 text-second">Skills</h5>

              <hr className="my-0" />
            </div>

            <ul className="list-group">
              <li className="list-group-item">React (include Next js)</li>
              <li className="list-group-item">Vue (include Nuxt js)</li>
              <li className="list-group-item">Typescript</li>
              <li className="list-group-item">Laravel</li>
              <li className="list-group-item">Node js (Expres Js)</li>
            </ul>
          </div>
          <div>
            <div className="skills ">
              <h5 className="mb-0 text-second">Experience</h5>
              <hr className="my-0" />
            </div>
            <ul className="list-group">
              <li className="list-group-item">
                FrontEnd Developer Intern In Asian Venture Philanthropy Network
                (AVPN) Base Singapore
              </li>
              <li className="list-group-item">
                FrontEnd Developer Path Generasi Gigih 2 By Gojek 2022
              </li>
              <li className="list-group-item">
                Mobile And FrontEnd Developer Path Generasi Idcamp
                2019,2020,2021,2022
              </li>
              <li className="list-group-item">
                FrontEnd Developer Path Generasi Baparekraf Developer Day 2020,
                2021
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
