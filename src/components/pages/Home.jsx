import React, { useEffect, useState } from "react";
import BannerOne from "../Container/BannerOne";
import BannerTwo from "../Container/BannerTwo";
import BannerThree from "../Container/BannerThree";
import BannerFour from "../Container/BannerFour";
import { useSelector } from "react-redux";
const Home = () => {
  const { pokemon } = useSelector((state) => state);
  const [data, setData] = useState([]);
  useEffect(() => {
    document.title = "home";
    if (pokemon) {
      setData(pokemon);
    }
  }, [pokemon]);

  return (
    <div>
      <BannerOne class="bannerone" />
      <BannerTwo class="bannertwo" />
      <BannerThree class="bannerthree" data={data} />
      <BannerFour class="bannerfour" />
    </div>
  );
};

export default Home;
