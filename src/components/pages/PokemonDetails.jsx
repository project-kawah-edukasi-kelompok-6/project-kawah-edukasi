import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Images } from "../atoms";
import NotFound from "./NotFound";

const PokemonDetails = () => {
  const params = useParams();
  const [pokemon, setPokemon] = useState({});
  const [notfound, setnotfound] = useState(false);
  const getPokemon = () => {
    axios
      .get(` https://pokedexbackend.herokuapp.com/pokemon/${params.id}`)
      .then((response) => {
        setPokemon(response?.data?.result || {});
      })
      .catch((e) => {
        setnotfound(true);
      });
  };

  useEffect(() => {
    document.title = pokemon?.name;
    getPokemon();
  }, [getPokemon]);
  return (
    <div className="main row justify-content-center p-4">
      {notfound ? (
        <NotFound title={"Pokemon Tidak Ditemukan"} />
      ) : (
        <>
          {Object.keys(pokemon).length ? (
            <div className="row justify-content-center">
              <div className="col-md-2">
                <Images src={`${pokemon?.image}`} className="img-thumbnail" />
                <div className="mt-3"></div>
              </div>
              <div className="col-md-6">
                <h3 className="mb-0">{pokemon?.name}</h3>
                <h6 className="text-muted mb-3">
                  {pokemon?.types?.map((v) => (
                    <span
                      key={`${v}${pokemon?.name}`}
                      className={`badge text-bg-primary me-2 ${v.toLowerCase()}`}
                    >
                      {v}
                    </span>
                  ))}
                </h6>
                <div>
                  <div className="skills">
                    <h5 className="mb-0 text-second">Abilities</h5>

                    <hr className="my-0" />
                  </div>

                  <ul className="list-group">
                    {pokemon?.abilities?.map((v) => (
                      <li
                        key={`${v}${pokemon?.name}`}
                        className="list-group-item"
                      >
                        {v}
                      </li>
                    ))}
                  </ul>
                </div>
                <div>
                  <div className="skills ">
                    <h5 className="mb-0 text-second">Experience</h5>
                    <hr className="my-0" />
                  </div>
                  <ul className="list-group">
                    {pokemon?.stats?.map((v) => (
                      <li key={`${v.name}`} className="list-group-item stat">
                        <p className="text-uppercase"> {v.name} </p>
                        <p>{v.value} </p>
                      </li>
                    ))}
                  </ul>
                </div>
              </div>
            </div>
          ) : (
            <div className="loading">
              <div className="spinner-border text-primary" role="status"></div>
              <span className="visually text-primary ms-4">Loading...</span>
            </div>
          )}
        </>
      )}
    </div>
  );
};

export default PokemonDetails;
