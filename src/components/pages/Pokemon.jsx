import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { Cards } from "../molekul";
import Search from "../molekul/Search";
import { useDispatch, useSelector } from "react-redux";
import { FavPokemon } from "../../bootstrap/actions";
const Pokemon = () => {
  const dispacth = useDispatch();
  const [querypokemon, setqueryPokemon] = useState("");
  const [isSearch, setIsSearch] = useState(false);
  const location = useLocation();
  const { pokemon, favorite } = useSelector((state) => state);
  const [data, setData] = useState([]);
  const [pagination, setpagiantion] = useState({
    page: 0,
    limit: 18,
    lengdata: 0,
  });
  const navpagination = (next = true) => {
    if (next) {
      setpagiantion({
        ...pagination,
        page: pagination.page + 1,
      });
    } else {
      if (pagination.page >= 0) {
        setpagiantion({
          ...pagination,
          page: pagination.page - 1,
        });
      }
    }
  };

  const createPaginate = (pokemons, page_size, page_number) => {
    const pokemon = pokemons.map((pokemons) => ({
      ...pokemons,
      isFavorite: favorite.find((sele) => sele.id === pokemons.id)
        ? true
        : false,
    }));
    return pokemon.slice(
      page_number * page_size,
      page_number * page_size + page_size
    );
  };

  const search = (e) => {
    const { name, value } = e.target;
    console.log(value);
    setqueryPokemon(value);
    if (value.length) {
      const searchdata = pokemon.filter((element) =>
        element.name.toLowerCase().includes(value.toLowerCase())
      );
      setIsSearch(true);
      const newData = createPaginate(
        searchdata,
        pagination.limit,
        pagination.page
      );
      setData(newData);
      setpagiantion({
        ...pagination,
        lengdata: searchdata.length,
      });
    } else {
      setIsSearch(false);
      const paginate = createPaginate(
        pokemon,
        pagination.limit,
        pagination.page
      );
      setData(paginate);
    }
  };
  const setFavorite = (data) => {
    dispacth(FavPokemon({ favorite, data }));
  };
  useEffect(() => {
    const title = location.pathname.split("/");
    document.title = title[1];
    if (pokemon && !isSearch) {
      const paginate = createPaginate(
        pokemon,
        pagination.limit,
        pagination.page
      );
      setData(paginate);
    } else if (isSearch) {
      const searchdata = pokemon.filter((element) =>
        element.name.toLowerCase().includes(querypokemon.toLowerCase())
      );
      setIsSearch(true);
      const newData = createPaginate(
        searchdata,
        pagination.limit,
        pagination.page
      );
      setData(newData);
    }
  }, [pokemon, pagination, isSearch, favorite]);

  const pokemons = data?.length ? (
    data?.map((data) => {
      return (
        <div
          key={data.id}
          className="col-12 col-md-2 d-flex justify-content-center"
        >
          <Cards
            id={data.name}
            ids={data.id}
            title={data?.name}
            data={data}
            flipper={true}
            favorite={data.isFavorite}
            onSetFavorite={setFavorite}
            dualimages={{
              front: data?.pictureFront,
              back: data?.pictureBack,
            }}
            api={true}
          />
        </div>
      );
    })
  ) : (
    <div className="loading">
      <div className="spinner-border text-primary" role="status"></div>
      <span className="visually text-primary ms-4">Loading...</span>
    </div>
  );

  return (
    <div className="main row justify-content-center p-4 me-0">
      <div className="col-10 col-md-6">
        <Search onChange={search} />
      </div>
      <div className="col-12 row">{pokemons}</div>
      <>
        {data?.length ? (
          <div className="col-10 mt-5 d-flex justify-content-center pagination-container">
            <div className="btn-group" role="group" aria-label="Basic example">
              <button
                onClick={() => navpagination(false)}
                type="button"
                className="btn btn-primary"
                disabled={pagination.page === 0}
              >
                Previous
              </button>
              <button type="button" className="btn btn-primary" disabled>
                {`Page : ${pagination.page === 0 ? 1 : pagination.page + 1} of 
             ${
               Math.floor(
                 isSearch ? pagination.lengdata / 18 : pokemon.length / 18
               ) + 1
             }`}
              </button>
              <button
                onClick={() => navpagination()}
                type="button"
                className="btn btn-primary"
                disabled={
                  pagination.page ===
                  Math.floor(
                    isSearch ? pagination.lengdata / 18 : pokemon.length / 18
                  )
                }
              >
                Next
              </button>
            </div>
          </div>
        ) : (
          <div></div>
        )}
      </>
    </div>
  );
};

export default Pokemon;
