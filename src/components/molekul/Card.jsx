import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Images } from "../atoms";
const Card = ({
  badge = [],
  api = false,
  hidden = false,
  flipper = false,
  favorite = false,
  ...props
}) => {
  const [flip, setflip] = useState(false);
  const navigate = useNavigate();
  useEffect(() => {}, [flip]);
  return (
    <div
      onMouseOver={() => setflip(true)}
      onMouseLeave={() => setflip(false)}
      className="card justify-content-center pb-2 text-center"
    >
      <div onClick={() => navigate(`/pokemon/${props.ids}`)}>
        <Images
          api={api}
          src={
            flipper
              ? flip
                ? props.dualimages.front
                : props.dualimages.back
              : props.image
          }
          alt="img-card"
          className="card-img-top"
        />
        <div className="card-body">
          <h5 className="card-title">{props.title}</h5>
          <h6 className="card-subtitle mb-2 text-muted">
            {badge?.map((v) => (
              <span
                key={`${v.slot}${v.title}`}
                className={`badge text-bg-primary me-2 ${v.type.name}`}
              >
                {v.type.name}
              </span>
            ))}
          </h6>
          <p className="card-text">{props?.subtitle}</p>
        </div>
      </div>
      <div className="d-flex justify-content-center">
        <button
          hidden={hidden}
          onClick={() => props.onSetFavorite(props.data)}
          className={`btn ${favorite ? "btn-danger" : "btn-success"}`}
        >
          {favorite ? "Remove Favorite" : "Add Favorite"}
        </button>
      </div>
    </div>
  );
};

export default Card;
