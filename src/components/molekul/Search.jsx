import React from "react";
import { useState } from "react";

const Search = ({ onChange }) => {
  const [searchInput, setSearchInput] = useState("");

  return (
    <div className="input-group mb-3">
      <input
        type="text"
        onChange={(e) => onChange(e)}
        className="form-control"
        placeholder="Insert Name Pokemon"
        aria-label="Insert Name Pokemon"
        aria-describedby="button-addon2"
        name="search"
      />
      <button
        className="btn btn-outline-secondary"
        type="button"
        id="button-addon2"
        disabled={true}
      >
        Search
      </button>
    </div>
  );
};

export default Search;
