import React from "react";
import { Images } from "../atoms";

const Footer = () => {
  return (
    <div className="footer">
      <div>Kawah Edukasi</div>
      <a
        href="https://www.linkedin.com/in/mhirvan/"
        target="_blank"
        rel="noreferrer"
      >
        <Images api={false} src="linkedin.png" />
      </a>
    </div>
  );
};

export default Footer;
